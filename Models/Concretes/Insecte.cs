﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Concretes
{
    public class Insecte
    {
        public string Icone { get; set; }
        public string Nom { get; set; }
        public string Relation { get; set; }
    }
}
