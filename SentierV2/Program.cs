﻿using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using Models;
using Models.Concretes;
using Models.Enums;
using Models.Interfaces;
using Models.Services;

namespace SentierV2
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 1ere demo
            //using (SqlConnection connection = new SqlConnection())
            //{
            //    // windows auth
            //    // data source=[server name];initial catalog=[db name];integrated security=true
            //    // SQL auth
            //    // data source=[server name];initial catalog=[db name];uid=[username];pwd=[password]
            //    connection.ConnectionString = @"data source=K-PC;initial catalog=Sentier;integrated security=true";
            //    // ouverture de la connection au server SQL
            //    connection.Open();

            //    string query = 
            //        @"SELECT 
            //            * 
            //          FROM Plante p
            //          JOIN Traduction t ON p.TraductionId = t.Id
            //          JOIN Coordonnee c ON p.CoordonneeId = c.Id
            //        ";

            //    SqlCommand cmd = connection.CreateCommand();
            //    cmd.CommandText = query;

            //    // Créer un SQLDataReader que l'on va pouvoir parcourir pour récupérer un ensemble de lignes
            //    // cmd.ExecuteReader();

            //    // Exécuter une commande et de récupérer le nombre de lignes qui ont été modifées
            //    // cmd.ExecuteNonQuery();

            //    // Exécuter une commande et lire la première donnée qui est retournée
            //    // cmd.ExecuteScalar();

            //    SqlDataReader reader = cmd.ExecuteReader();
            //    while(reader.Read())
            //    {
            //        Console.WriteLine($"La plante {reader["NomFr"]} se trouve en ({reader["Lat"]}, {reader["Long"]})");
            //        // faire qqchose
            //    }

            //    // fermeture de la connection
            //    connection.Close();
            //} 
            #endregion
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = @"data source=K-PC;initial catalog=Sentier;integrated security=true";
                PlanteService service = new PlanteService(connection);
                List<PlanteBasique> plantes = service.GetPlantesBasiques(ESaison.Automne);
                foreach(PlanteBasique plante in plantes)
                {
                    DisplayFr(plante);
                    Console.WriteLine(
                        $"Cette plante se trouve en ({plante.Coordonnee.Latitude} | {plante.Coordonnee.Longitude})"
                    );
                }
            }

        }

        static void DisplayEn(IAnglais plante)
        {
            Console.WriteLine($"{plante.Nom}");
            Console.WriteLine($"{plante.Description}");
        }

        static void DisplayFr(IFrancais plante)
        {
            Console.WriteLine($"{plante.Nom}");
            Console.WriteLine($"{plante.Description}");
        }

        static void DisplayNl(INeerlandais plante)
        {
            Console.WriteLine($"{plante.Nom}");
            Console.WriteLine($"{plante.Description}");
        }
    }
}
    