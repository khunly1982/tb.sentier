﻿using Models.Abstraites;
using Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Concretes
{
    public class PlanteBota : PlanteInitie
    {
        public List<EStatut> Statuts { get; set; }
    }
}
