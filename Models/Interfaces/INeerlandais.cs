﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Interfaces
{
    public interface INeerlandais
    {
        string Nom { get; set; }
        string Description { get; set; }
    }
}
