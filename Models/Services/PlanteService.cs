﻿using Models.Concretes;
using Models.Enums;
using Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace Models.Services
{
    public class PlanteService
    {
        private DbConnection _connection;

        // Injection de dépendances
        public PlanteService(DbConnection connection)
        {
            _connection = connection;
        }

        public List<PlanteBasique> GetPlantesBasiques(ESaison saison)
        {
            // ouvrir la connection
            _connection.Open();
            // créer une commande
            DbCommand cmd = _connection.CreateCommand();
            cmd.CommandText = @"SELECT 
                        * 
                      FROM Plante p
                      JOIN Traduction t ON p.TraductionId = t.Id
                      JOIN Coordonnee c ON p.CoordonneeId = c.Id
                      WHERE p.Id = any (
	                    SELECT PlanteId 
	                    FROM PlanteSaison
	                    WHERE SaisonId = @p1
                      )
                    ";
            DbParameter pa = cmd.CreateParameter();
            pa.ParameterName = "p1";
            pa.Value = (int)saison;
            cmd.Parameters.Add(pa);
            // executer la commande
            DbDataReader reader = cmd.ExecuteReader();

            List<PlanteBasique> plantes = new List<PlanteBasique>();

            // parcourir le reader
            while(reader.Read())
            {
                // remplir la liste
                PlanteBasique p = new PlanteBasique();
                // remplir les propriétés de ma plante
                // hydrater l'objet vide // mapping de reader => p
                p.Coordonnee = new Coordonnee((double)reader["Long"], (double)reader["Lat"])
                {
                    Altitude = reader["Alt"] as double?
                };
                ((IAnglais)p).Nom = reader["NomEn"] as string;
                ((IAnglais)p).Description = reader["DescriptionEn"] as string;
                ((IFrancais)p).Nom = reader["NomFr"] as string;
                ((IFrancais)p).Description = reader["DescriptionFr"] as string;
                ((INeerlandais)p).Nom = reader["NomNl"] as string;
                ((INeerlandais)p).Description = reader["DescriptionNl"] as string;

                plantes.Add(p);
            }
            // fermer la connection
            _connection.Close();
            return plantes;
        }

        public List<PlanteInitie> GetPlantesInities()
        {
            return null;
        }

        public List<PlanteBota> GetPlantesBota()
        {
            return null;
        }
    }
}
