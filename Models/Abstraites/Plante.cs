﻿using Models.Concretes;
using Models.Enums;
using Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Abstraites
{
    public abstract class Plante : IAnglais, IFrancais, INeerlandais, ILocalisable
    {
        public Coordonnee Coordonnee { get; set; }
        string IAnglais.Nom { get; set; }

        string IFrancais.Nom { get; set; }

        string INeerlandais.Nom { get; set; }
        

        string IAnglais.Description { get; set; }

        string IFrancais.Description
        {
            get; set;
        }

        string INeerlandais.Description
        {
            get; set;
        }

        public List<ESaison> Saisons { get; set; }
    }
}
