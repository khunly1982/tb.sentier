﻿using Models.Abstraites;
using Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Concretes
{
    public class PlanteInitie : PlanteBasique
    {
        public List<EVertu> Vertus { get; set; }
        public string NomLatin { get; set; }
    }
}
